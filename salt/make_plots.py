import h5py
import numpy as np
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger

networks = {
    "SubjetXbb" : "/user/wmorren/project/salt/salt/logs/SubjetXbb_20230215-T181721/ckpts/epoch=001-val_loss=0.95792__test_Mix.h5",
    #"GN1Xbb" : "/eos/home-u/umami/training-samples/gnn/xbb/pretrained-GN1Xbb/ckpts/epoch=015-val_loss=0.61651__test_Mix.h5"
}
reference = "SubjetXbb"

#test_path = '/eos/home-u/umami/training-samples/gnn/xbb/inclusive_testing_Mix.h5'
test_path = '/dcache/atlas/higgs/ggilles/SALT_Tutorial/inclusive_testing_Mix.h5'

def disc_fct(arr: np.ndarray, f_c: float = 0.018) -> np.ndarray:
    """Tagger discriminant

    Parameters
    ----------
    arr : numpy.ndarray
        array with with shape (, 3)
    f_c : float, optional
        f_c value in the discriminant (weight for c-jets rejection)

    Returns
    -------
    np.ndarray
        Array with the discriminant values inside.
    """
    # you can adapt this for your needs
    return np.log(arr[2] / (f_c * arr[1] + (1 - f_c) * arr[0]))

with h5py.File(test_path, 'r') as test_f:
    jet_pt = test_f['jets']['pt'] / 1000
    jet_mass = test_f['jets']['mass'] / 1000
    jet_eta = np.abs(test_f['jets']['eta'])
    flav = test_f['jets']['R10TruthLabel_R22v1']
    mask = (jet_pt < 1000) & (jet_pt > 250) & (jet_mass > 50) & (jet_mass < 300)
    is_QCD = flav == 10
    is_Hcc = flav == 12
    is_Hbb = flav == 11
    is_Top = flav == 1
    n_jets_QCD = np.sum(is_QCD & mask)
    n_jets_Top = np.sum(is_Top & mask)

results = {}
logger.info("Calculate discriminants and rejection")
for key, val in networks.items():
    with h5py.File(val, 'r') as f:
        pHbb = f['jets']['salt_pHbb']
        pHcc = f['jets']['salt_pHcc']
        pQCD = f['jets']['salt_pdi']
        pTop = f['jets']['salt_ptop']
        disc_Hbb = pHbb
        disc_Hcc = pHcc


        sig_eff = np.linspace(0.4, 1, 100)
        Hbb_rej_QCD = calc_rej(disc_Hbb[is_Hbb & mask], disc_Hbb[is_QCD & mask], sig_eff)
        Hbb_rej_Top = calc_rej(disc_Hbb[is_Hbb & mask], disc_Hbb[is_Top & mask], sig_eff)
        Hcc_rej_QCD = calc_rej(disc_Hcc[is_Hcc & mask], disc_Hcc[is_QCD & mask], sig_eff)
        Hcc_rej_Top = calc_rej(disc_Hcc[is_Hcc & mask], disc_Hcc[is_Top & mask], sig_eff)
        results[key] = {
            'sig_eff' : sig_eff,
            'disc_Hbb' : disc_Hbb,
            'disc_Hcc' : disc_Hcc,
            'Hbb_rej_QCD' : Hbb_rej_QCD,
            'Hbb_rej_Top' : Hbb_rej_Top,
            'Hcc_rej_QCD' : Hcc_rej_QCD,
            'Hcc_rej_Top' : Hcc_rej_Top
        }


logger.info("Plotting Discriminants.")
plot_histo = {
    key : HistogramPlot(
        n_ratio_panels=1,
        ylabel="Normalised number of jets",
        xlabel=f"{key}-jet discriminant",
        logy=True,
        leg_ncol=1,
        figsize=(6.5, 4.5),
        bins=np.linspace(0, 1, 50),
        y_scale=1.5,
        ymax_ratio_1=2,
        ymin_ratio_1=0.2,
        atlas_second_tag="$\\sqrt{s}=13$ TeV, Xbb jets",
    ) for key in ['Hbb', 'Hcc']}
linestyles = get_good_linestyles()[:len(networks.keys())]
colours = get_good_colours()[:3]
for key, value in plot_histo.items():
    for network, linestyle in zip(networks.keys(), linestyles):
        value.add(
            Histogram(
                results[network][f'disc_{key}'][is_QCD],
                label="QCD jets" if network == reference else None,
                ratio_group="QCD",
                colour=colours[0],
                linestyle=linestyle,
            ),
            reference=(network == reference),
            )
        value.add(
            Histogram(
                results[network][f'disc_{key}'][is_Top],
                label="Top jets" if network == reference else None,
                ratio_group="Top",
                colour=colours[1],
                linestyle=linestyle,
            ),
            reference=(network == reference),
            )
        value.add(
            Histogram(
                results[network][f'disc_{key}'][is_Hbb if key == 'Hbb' else is_Hcc],
                label=f"{key} jets" if network == reference else None,
                ratio_group=f"{key}",
                colour=colours[2],
                linestyle=linestyle,
            ),
            reference=(network == reference),
            )
    value.draw()
    # The lines below create a legend for the linestyles
    value.make_linestyle_legend(
        linestyles=linestyles, labels=networks.keys(), bbox_to_anchor=(0.5, 1)
    )
    value.savefig(f"disc_{key}.png", transparent=False)

# here the plotting of the roc starts
logger.info("Plotting ROC curves.")
plot_roc = {
    key : RocPlot(
        n_ratio_panels=2,
        ylabel="Background rejection",
        xlabel=f"{key}-jet efficiency",
        atlas_second_tag="$\\sqrt{s}=13$ TeV, Xbb jets",
        figsize=(6.5, 6),
        y_scale=1.4,
    ) for key in ['Hbb', 'Hcc']}

for key, value in plot_roc.items():
    for network in networks.keys():
        value.add_roc(
            Roc(
                sig_eff,
                results[network][f'{key}_rej_QCD'],
                n_test=n_jets_QCD,
                rej_class="QCD",
                signal_class=f"{key}",
                label=f"{network}",
            ),
            reference=(reference == network),
        )
        value.add_roc(
            Roc(
                sig_eff,
                results[network][f'{key}_rej_Top'],
                n_test=n_jets_Top,
                rej_class="Top",
                signal_class=f"{key}",
                label=f"{network}",
            ),
            reference=(reference == network),
        )
    # setting which flavour rejection ratio is drawn in which ratio panel
    value.set_ratio_class(1, "QCD", label="QCD-jet ratio")
    value.set_ratio_class(2, "Top", label="Top-jet ratio")
    value.set_leg_rej_labels("QCD", "QCD-jet rejection")
    value.set_leg_rej_labels("Top", "Top-jet rejection")
    value.draw()
    value.savefig(f"roc_{key}.png", transparent=False)