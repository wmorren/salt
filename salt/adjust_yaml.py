import yaml
import numpy as np
from argparse import ArgumentParser
#from configparser import ConfigParser

def MHARegression_training(N, j, v, w, t, H, D, L, n, e, r, d, b):
    yaml_file = "/user/wmorren/project/salt/salt/configs/regressionHbb_copy.yaml"

    with open(yaml_file, 'r') as f:
        sc = yaml.load(f, Loader=yaml.FullLoader)

    sc["name"] = N #Name of the folder
    sc["data"]["num_jets_train"] = j #number of jets for training
    sc["data"]["num_jets_val"] = v #number of jets for validation
    sc["data"]["num_jets_test"] = w #number of jets for validation
    #tracks?
    sc["data"]["batch_size"] = b #number of jets for validation
    sc["model"]["lrs_config"]["initial"] = r

    print("Using",j*t*10*32/1e9,"GB of RAM for the tracks and", b*t*10*32/1e9,"GB of RAM per batch")

    model_dict = sc["model"]["model"]["init_args"]
    gnn = model_dict["gnn"]["init_args"]
    gnn["mha_config"]["num_heads"] = H #dim_head = emb_dim / num_heads
    gnn["num_layers"] = L #number of mha layers

    submodel = model_dict["init_nets"]["init_args"]["modules"][0] #0=tracks #Now only tracks are included
    #s = submodel["init_args"]["net"]["init_args"]["input_size"] #Number of track variables
    #h = submodel["init_args"]["net"]["init_args"]["hidden_layers"][0] #transofrmed via hidden layers
    submodel["init_args"]["net"]["init_args"]["output_size"] = D #embedding of track variables & dk, dv, dq
    gnn["embed_dim"] = D
    model_dict["pool_net"]["init_args"]["input_size"] = D

    task = model_dict["tasks"]["init_args"]["modules"][0]
    task["init_args"]["net"]["init_args"]["input_size"] = D
    if (n>=np.log(D)/np.log(2)):
        raise ValueError(f"Having {n} dense layers causes the size of the last hidden layer to be less or equal to 1, since {n} is being divided by {np.log(D)/np.log(2)}.")
    h=[int(D/2**i) for i in range(n)]
    task["init_args"]["net"]["init_args"]["hidden_layers"] = h #hidden dense layers after MHA
    
    sc["trainer"]["max_epochs"] = e #max number of epochs
    #learning rate?
    gnn["dense_config"]["dropout"] = d #dropout rate
    task["init_args"]["net"]["init_args"]["dropout"] = d

    yaml_file = f"/user/wmorren/project/salt/salt/logs/{N}/regressionHbb.yaml"
    with open(yaml_file, 'w+') as f:
        sc = yaml.dump(sc, stream=f,
                       default_flow_style=None, sort_keys=False)#default_flow_style=False

#if a python file was imported by this file, then this other file is not the main, only this one is, making it able to parse arguments.
if __name__ == "__main__":
    
    parser = ArgumentParser(description = "run MHARegression training")
    parser.add_argument("-N", "--name", action = "store", dest = "N", default = "reg", help = "name of the folder")
    parser.add_argument("-j", "--njets_train", action = "store", dest = "j", default = 1000, help = "number of jets requested for the training", type=int)
    parser.add_argument("-v", "--njets_val", action = "store", dest = "v", default = 1000, help = "number of jets requested for the validation", type=int)
    parser.add_argument("-w", "--njets_test", action = "store", dest = "w", default = 1000, help = "number of jets requested for the testing", type=int)    
    parser.add_argument("-t", "--ntracks", action = "store", dest = "t", default = 100, help = "number of tracks per jets considered for the training", type=int)
    parser.add_argument("-H", "--nheads", action = "store", dest = "H", default = 2, help = "number of requested heads in MHA layer", type=int)
    parser.add_argument("-D", "--dim_heads", action = "store", dest = "D", default = 64, help = "embedding dimension, which will be splitted over the heads", type=int)#size of each attention head for query and key")
    parser.add_argument("-L", "--nMHAlayers", action = "store", dest = "L", default = 1, help = "number of MHA layers", type=int)
    parser.add_argument("-n", "--nDlayers", action = "store", dest = "n", default = 1, help = "number of DNN hidden layers", type=int)
    parser.add_argument("-e", "--nepochs", action = "store", dest = "e", default = 200, help = "number of training epochs", type=int)
    parser.add_argument("-r", "--learning_rate", action = "store", dest = "r", default = 1e-5, help = "learning rate", type=float)
    parser.add_argument("-d", "--vdropout", action = "store", dest = "d", default = 0.5, help = "dropout rate", type=float)
    parser.add_argument("-b", "--batch", action = "store", dest = "b", default = 128, help = "batch size", type=int)
    #parser.add_argument("--training_filepath", action = "store", dest = "training_filepath", help = "training filepath")
    #parser.add_argument("--outputdir", action = "store", dest = "outputdir", help = "training output directory")
    args = vars(parser.parse_args())

    MHARegression_training(**args)

