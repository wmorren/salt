output_path="/user/wmorren/project/salt/salt/logs"
t=$(date +%Y%m%d_%H%M%S)
 
# options
Job="sub_${t}"

# setup output directory
outputdir="${output_path}/${Job}"
mkdir -p ${outputdir}

# create script
Script="${outputdir}/${Job}.sh"
rm -rf ${Script}*
touch ${Script}

echo "source /user/wmorren/data/MyPython/bin/activate" >> ${Script}
#echo "salt fit --config configs/SubjetXbb.yaml --data.num_workers 6 --trainer.fast_dev_run 2" >> ${Script}
#echo "salt fit --config configs/SubjetXbb.yaml --data.num_workers 6 --data.move_files_temp /tmp" >> ${Script}
echo "salt fit --config /user/wmorren/project/salt/salt/configs/SubjetXbb.yaml --data.num_workers 6 --trainer.accelerator cpu --name ${Job}" >> ${Script}

#echo "salt test --config /user/wmorren/project/salt/salt/logs/SubjetXbb/config.yaml --data.num_workers 6 --trainer.accelerator cpu" >> ${Script}

source ${Script}
#express 10m, short 4h, generic 24h, long 48h
#qsub -q express -N "t${t}" -o "${outputdir}/log_output.o" -e "${outputdir}/log_error.e" ${Script} #${Job}
#qsub -q short -l nodes=1:ppn=2 -N ${Job} -o "${outputdir}/log_output.o" -e "${outputdir}/log_error.e" ${Script}
#qsub -q long -l walltime=72:00:00 -w e -N ${Job} -o "${outputdir}/log_${Job}.o" -e "${outputdir}/log_${Job}.e" ${Script}

#rm -rf ${Script}*