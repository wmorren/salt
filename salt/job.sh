#########################
#       Job inputs      #
#########################

# Path of the training .h5 file
#training_filepath="/data/atlas/users/ggilles/VHcc/FlatMassSamples/umami_preprocessed/BoostedMix_training-resampled.h5"
train_folder="/dcache/atlas/higgs/ggilles/boosted_Xcc"
train_subfolder="/user.ggilles.801471.e8441_e7400_s3681_r13144_r13146_p5057.tdd.FatJets.22_2_82.22-12-12-T232252_output.h5"
train_file="/user.ggilles.31581424._000001.output.h5" #Merged or multiple files, e.g. 2 for train, 1 for val, 1 for test?
train_filepath="${train_folder}${train_subfolder}"
#${train_file}"

#UFO Hbb
#train_subfolder_Hbb="/user.dkobylia.801471.e8441_s3681_r13144_p5488.tdd.FatJets.22_2_100.23-01-12_test-xbb_output.h5"
#UFO Hcc
#train_subfolder_Hcc="/user.dkobylia.801472.e8441_s3681_r13144_p5488.tdd.FatJets.22_2_100.23-01-12_test-xbb_output.h5"
#train_filepath="/Merged.h5"
#train_filepath="${train_folder}${train_subfolder_Hbb}${train_filepath}"

# Path of the Validation .h5 file
val_folder="/dcache/atlas/higgs/ggilles/boosted_Xcc"
val_subfolder="/user.ggilles.801471.e8441_e7400_s3681_r13144_r13146_p5057.tdd.FatJets.22_2_82.22-12-12-T232252_output.h5"
val_file="/user.ggilles.31581424._000003.output.h5"
val_filepath="${val_folder}${val_subfolder}"
#${val_file}"

# Path of the output repository
output_path="/user/wmorren/project/salt/salt/logs"

# Number of jets
njets=(3) #(100000)
njetsval=2 #25000
njetstest=1 #-1 #Add isHbb in MakePLots!
# Number of tracks #(30)
ntracks=(100)
# Number of multihead attention layers
nMHAlayers=(1)
# Number of head per multi-head attention layer #W:(2 4 8 16) #G:(10 50 100) num_heads>emb_dim as num_heads*dim_head=emb_dim, else: raiserror omits the model
nheads=(2)
# Size of each attention head for query and key #W:(20 40 80 160) #D:128 #G:(5 10 30) #Same as or larger than nfeatures tracks=10 * num_heads
dim_emb=(20)
# Number of densehidden layer in the Deep Neural Network #W:(1 3 5) #G:(3 5), input=emb_dim+jetfeatures, size hidden layers: emb_dim/2^n for n in range(Dlayer)
nDlayers=(1)
# Number of training epochs: as many as is needed
nepochs=(2)
# Learning rates #W:(1e-3 1e-4 1e-5 1e-6 1e-7 1e-8)
learning_rate=(1e-4)
# Drop out factors #W:(1e-1 2e-1 3e-1 4e-1 5e-1)
drop=(1e-1)
# Batch size #W:(32 128 512 2048 8192 32769 131072) #(10 32 64 128 256 512 1024 2048 4096 8192 16384 32769 65536 131072) or as large as reasonbly possible = 245000 ->2^17=131072
batch=(256)
#--njets 50000 --ntracks 30 --nheads 3 --nlayers 5 --nepochs 10 --learning_rate 1e-5 --vdropout 0.2 --batch 10

# Loop over number of jets
for j in "${njets[@]}"
do
    # Loop over the number of tracks
    for t in "${ntracks[@]}"
    do
        # Loop over the number of multihead attention layer
        for L in "${nMHAlayers[@]}"
        do
            # Loop over number of heads
            for H in "${nheads[@]}"
            do
                # Loop over the sizes of each attention head for query and key
                for D in "${dim_emb[@]}"
                do
                    # Loop over the number of dense layers
                    for n in "${nDlayers[@]}"
                    do
                        # Loop over the number of training epochs
                        for e in "${nepochs[@]}"
                        do
                            # Loop over the learning rate values
                            for r in "${learning_rate[@]}"
                            do
                                # Loop over the drop out factor values
                                for d in "${drop[@]}"
                                do
                                    # Loop over the batch size values
                                    for b in "${batch[@]}"
                                    do
                                    
                                    # options
                                    timestamp=$(date +%Y%m%d_%H%M%S)
                                    Job="j${j}_t${t}_b${b}_r${r}_H${H}_D${D}_L${L}_n${n}_e${e}_d${d}_${timestamp}"

                                    c="-N reg_${Job} -j ${j} -v ${njetsval} -w ${njetstest} -t ${t} -b ${b} -r ${r} -H ${H} -D ${D} -L ${L} -n ${n} -e ${e} -d ${d}" #
                                    
                                    # setup output directory
                                    outputdir="${output_path}/reg_${Job}"
                                    mkdir -p ${outputdir}
                    
                                    # create script
                                    Script="${outputdir}/${Job}.sh"
                                    rm -rf ${Script}*
                                    touch ${Script}

                                    echo "source /user/wmorren/data/MyPython/bin/activate" >> ${Script}
                                    echo "python /user/wmorren/project/salt/salt/adjust_yaml.py ${c}" >> ${Script}
                                    nw=6 #cat /proc/cpuinfo | awk '/^processor/{print $3}' | tail -1 ->gives 5, but SALT gives 6 locally and on stoomboot cpu and gpu
                                    ac="gpu"

                                    #There is 65,5GB RAM: 8 cores of 8,2 GB
                                    #Need 5M/10^9*32*100=16 GB at least per training = 2 cpu cores
                                    #--data.num_workers 6 for gpu locally, 1 for cpu qsub, 5 for qsub gpu?
                                    #number of cores: can be more using nodes=1:ppn=2,3,...,32 =32/#jobs

                                    #echo "salt fit --config ${outputdir}/regressionHbb.yaml --data.num_workers ${nw} --trainer.fast_dev_run 2 --trainer.accelerator ${ac}" >> ${Script}
                                    #echo "salt fit --config ${outputdir}/regressionHbb.yaml --data.num_workers ${nw} --data.move_files_temp /tmp --trainer.accelerator ${ac}" >> ${Script}
                                    echo "salt fit --config ${outputdir}/regressionHbb.yaml --data.num_workers ${nw} --trainer.accelerator ${ac}" >> ${Script}
                                    
                                    #echo "salt test --config ${outputdir}/config.yaml --data.num_workers ${nw} --trainer.accelerator ${ac}" >> ${Script}
                                    
                                    echo "deactivate" >> ${Script}

                                    source ${Script}
                                    #Adjust bashrc to Comet log before running HPS!
                                    #express 10m, short 4h, generic 24h, long 48h
                                    #qsub -q gpu-nv -l 'nodes=1:ppn=1,walltime=00:05:00' -N ${timestamp} -o "${outputdir}/log_output.o" -e "${outputdir}/log_error.e" ${Script}
                                    #qsub -q gpu-nv -l 'nodes=1:ppn=1,walltime=00:10:00' -N ${timestamp} -o "${outputdir}/log_${Job}.o" -e "${outputdir}/log_${Job}.e" ${Script}
                                    
                                    #rm -rf ${Script}*
                                    done
                                done
                            done
                        done
                    done
                done
            done
        done
    done
done