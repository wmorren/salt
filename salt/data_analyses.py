import h5py
import numpy as np
import matplotlib.pyplot as plt
from puma import Histogram, HistogramPlot
import os

train_folder="/dcache/atlas/higgs/ggilles/boosted_Xcc"
train_subfolder="/user.dkobylia.801471.e8441_s3681_r13144_p5488.tdd.FatJets.22_2_100.23-01-12_test-xbb_output.h5"
train_file="user.dkobylia.31871096._000001.output.h5"
train_filepath=f"{train_folder}/{train_subfolder}/{train_file}"
#--njets 1154204 --ntracks 80 --nheads 3 --dim_heads 20 --nMHAlayers 1 --nDlayers 1 --nepochs 100 --learning_rate 1e-4 --vdropout 0.5 --batch 128

"""                           N         H==1
case tqqb:         return 1;  0         0
case Wqq:          return 2;  0         0
case Zbb:          return 3;  588       0
case Zcc:          return 4;  1346      0
case Zqq:          return 5;  12234     0
case Wqq_From_t:   return 6;  0         0
case other_From_t: return 7;  0         0
case other_From_V: return 8;  38325     0
case notruth:      return 9;  6232      0
case qcd:          return 10; 85071     155     0,01%
case Hbb:          return 11; 1401548   1397980 94%
case Hcc:          return 12; 7         6       0,0004%
case other_From_H: return 13; 85163     77219   5%
default:           return 0;
https://gitlab.cern.ch/atlas/athena/-/blob/master/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/ParticleJetTools/LargeRJetLabelEnum.h#L28
"""

def test(path):
    with h5py.File(path, 'r') as f:
        #jet_pt = f['jets']['pt'] / 1000
        #jet_mass = f['jets']['mass'] / 1000 #also used as input, this is not UFO jet reco mass
        #jet_mass_pred = f['jets']['mH_antikt10_pred'] / 1000 #Self definedc output
        #jet_mass_true = f['jets']['R10TruthLabel_R22v1_TruthJetMass'] / 1000 #Possible target value
        #jet_eta = np.abs(f['jets']['eta'])
        #flav = f['jets']['R10TruthLabel_R22v1']
        track_eta = np.abs(f['tracks']['eta'][:1])
        track_pt = f['tracks']['pt'][:1]/1000
        jet_pt = f['jets']['pt'][:1] / 1000
        print(np.nan_to_num(track_pt[0]), sum(np.nan_to_num(track_pt[0])), jet_pt)

def defs(path):
    with h5py.File(path, 'r') as f:
        n=-1#1000000
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count == 1
        #a=sum([count[i]==1 and flav[i] == 11 for i in range(n)])/n*100
        #b=sum([count[i]==1 and flav[i] != 11 for i in range(n)])/n*100
        #c=sum([count[i]==0 and flav[i] == 11 for i in range(n)])/n*100
        #d=sum([count[i]==0 and flav[i] != 11 for i in range(n)])/n*100
        #print(c, d, "\n", a, b)
        """
        The dataset contains all H->?->bb events, 
        Sometimes the anti-kt jet is not matched with a H, resulting in GhostHBosonsCount != 1.
        These jets are not suitible for training, as the jet probably does not contain the decay products of H.
        Hence they are left out, reducing the number of jets by 9.5% (14%), leaving us with 91.5% (86%) of the jets.

        There are also H->?->bb jets in which the H decayed into something else, after which two b were created.
        These are not labelled as Hbb jets and are hard to train on as it are completely different processes.
        Moreover, these few events only cause noise, so I want to leave them out (in iaddition to !=1) if possible.
        
           || 11   | !11  || t
        =========================
         0 || 0.2  | 9.3  || 9.5
         1 || 85.7 | 4.8  || 91.5
        =========================
         t || 85.9 | 14.1 || 100

        When choosing only Hbb events there are still a few events that are not HGost matched.............

        GhostHBosonsCount is either 0 or 1
        When flav = 11 => GhostHBosonsCount = 1.
        When GhostHBosonsCount = 1 =/=> flav = 11 (not always)
        The other way around is not always the case, 
        meaning that there are other events in which a H boson created the jet but did not result in Hbb.
        These are all event 13, which are other events resulting from H
        From 100 events: 87 Hbb, 0 Hcc, QCD 7, other_From_V 2 and 13 other_From_H
        #Can there be H->gg->bb events?
        """
        #for i in range(14):
        #    print(i, sum(flav[c] == i)/sum(c)*100)
        print(sum(is_Hbb)/len(is_Hbb)*100)
        #print(sum(is_Hbb), sum(c), flav[c])

def tracks2(path):
    with h5py.File(path, 'r') as f:
        n=-1#100
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        track_pt = f['tracks']['pt'][:n]/1000
        #track_eta = np.abs(f['tracks']['eta'][:1])
    is_Hbb = flav == 11
    c = count==1 

    N = np.array([sum(tracks>0.) for tracks in track_pt])
    N2 = np.array([sum(tracks>0.) for tracks in track_pt[is_Hbb]])
    N3 = np.array([sum(tracks>0.) for tracks in track_pt[c]])

    x=np.arange(0,100,10)#A few events have no tracks
    p=[sum(N3>i)/len(N3)*100 for i in x]
    plot = plt.figure(figsize=[6,5])
    plt.plot(x, p, ".-")
    plt.xlabel("x = Number of tracks per event")
    plt.ylabel("Percentage of jets with more than x tracks")
    plt.xlim(0,100)
    for i in range(len(p)):
        plt.text(x[i],p[i],round(p[i],3))
    plt.yscale("log")
    
    outputdir="plots"
    outputname="ptracks_distr.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)
    """
    plot = plt.figure(figsize=[10,5])
    plt.hist(N, bins=np.arange(0,100) , label="unfiltered", histtype=u'step')
    plt.hist(N2, bins=np.arange(0,100) , label="Hbb only", histtype=u'step')
    plt.hist(N3, bins=np.arange(0,100) , label="GhostHBosonsCount=1", histtype=u'step')
    plt.xlabel("Number of tracks per event")
    plt.ylabel("Number of jets")
    plt.yscale("log")
    plt.legend()
    
    outputname="ntracks2_distr.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)
    """

def ip_vs_pt(path):
    with h5py.File(path, 'r') as f:
        n=-1#1000
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count==1
        track_pt = np.nan_to_num((f['tracks']['pt'][:n]/1000)[c])
        track_ip = np.nan_to_num(np.abs(f['tracks']['IP3D_signed_d0_significance'][:n][c]))
    
    pt = [track_pt[:,i].mean() for i in range(100)]
    ip = [track_ip[:,i].mean() for i in range(100)]

    plot = plt.figure(figsize=[6,5])
    plt.scatter(pt, ip, marker=".")
    plt.xlabel("track_pt")
    plt.ylabel("track_ip")
    plt.yscale("log")
    plt.xscale("log")

    outputdir="plots"
    outputname="pi_vs_pt3.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)
    """
    plot = plt.figure(figsize=[6,5])
    plt.scatter(track_pt[0], track_ip[0], marker=".")
    plt.xlabel("track_pt")
    plt.ylabel("track_ip")
    plt.yscale("log")
    plt.xscale("log")

    outputdir="plots"
    outputname="pi_vs_pt4.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)
    """
def ip(path):
    with h5py.File(path, 'r') as f:
        n=1000
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count==1
        #track_pt = f['tracks']['pt'][:n]/1000
        track_ip = np.nan_to_num(f['tracks']['IP3D_signed_d0_significance'][:n][c])
        track_ip2 = np.nan_to_num(np.abs(f['tracks']['IP3D_signed_d0_significance'][:n][c]))

    ip = [track_ip[:,i].mean() for i in range(100)]
    ip2 = [track_ip2[:,i].mean() for i in range(100)]
    print(ip,ip2)

    plot = plt.figure(figsize=[6,5])
    plt.scatter(np.arange(100), ip, marker=".", label="IP")
    plt.scatter(np.arange(100), ip2, marker=".", label="abs(IP)")
    #print(len(track_pt[0]), len(np.arange(100))) #Only the first 22 tracks are printed as the first events only contains that many tracks.
    plt.xlabel("track number")
    plt.ylabel("average track_ip")
    plt.legend()
    #plt.axis('square')   
    #plt.yscale("log")
    #plt.xscale("log")
    #plt.ylim(1e-5, 40)

    outputdir="plots"
    outputname="IP3D_signed_d0_significance2.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def ip1(path):
    with h5py.File(path, 'r') as f:
        n=2
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count==1
        #track_pt = f['tracks']['pt'][:n]/1000
        track_ip = f['tracks']['IP3D_signed_d0_significance'][:n]
        track_ip = f['tracks']['d0'][:n]

    plot = plt.figure(figsize=[6,5])
    for ip in track_ip:
        plt.scatter(np.arange(100), ip, marker=".")
    #print(len(track_pt[0]), len(np.arange(100))) #Only the first 22 tracks are printed as the first events only contains that many tracks.
    plt.xlabel("track number")
    plt.ylabel("track_ip")
    #plt.axis('square')   
    #plt.yscale("log")
    #plt.xscale("log")
    #plt.ylim(1e-5, 40)

    outputdir="plots"
    outputname="d0.png"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def dip2(path):
    with h5py.File(path, 'r') as f:
        n=100
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count==1
        #track_pt = f['tracks']['pt'][:n]/1000
        track_ip = np.nan_to_num(f['tracks']['IP3D_signed_d0_significance'][:n])

    #ip = [(track_ip[:,i]-track_ip[:,i+1]).mean() for i in range(99)]
    for i in range(99):
        for j in range(n):
            if track_ip[j,i]<track_ip[j,i+1]:
                print("negative...")

def flow(path):
    with h5py.File(path, 'r') as f:
        n=1
        flav = f['jets']['R10TruthLabel_R22v1'][:n]
        count = f['jets']['GhostHBosonsCount'][:n]
        is_Hbb = flav == 11
        c = count==1
        flow_pt = f['flow']['pt'][:n]/1000
        track_pt = f['tracks']['pt'][:n]/1000
    for i in range(100):
        print(flow_pt[0][i], track_pt[0][i])

#tracks2(train_filepath)
ip1(train_filepath)
#flow(train_filepath)