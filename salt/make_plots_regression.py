import h5py
import numpy as np
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import os
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import seaborn as sb

def plotter_mass_dist(Y_val, Y_val_pred, jet_mass_UFO, normalize=True, outputdir='.'):
    """
    Plot mass distribution of the truth and predicted masses
    Normalization can be applied by setting 'normalize=True'
    """
    print("--Plotting mass distribution ")
    UFO = Histogram(jet_mass_UFO,label=r'$m_{UFO}$', colour="green")
    pred = Histogram(Y_val_pred,label=r'$m_{pred}$', colour="red")
    val = Histogram(Y_val,label=r'$m_{true}$', colour="dodgerblue")

    #initialise Histogram plot
    plot = HistogramPlot(
        xlabel=r'$m_{kin}$ [GeV]',
        ylabel="Normalised number of jets",
        bins=40,
        bins_range=(50,200),
        norm=normalize,
        logy=1,
        figsize=(6, 5),
        n_ratio_panels=1,
        atlas_second_tag="$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, \n$|\eta|$ < 1.97", #Not 100 GeV, as with Geoffreys script
    )
    
    plot.add(val, reference=True)
    plot.add(UFO)
    plot.add(pred)
    plot.draw()
    
    outputname="mass_distr.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_relam_dist(Y_val, Y_val_pred, normalize=True, outputdir='.'):
    """
    Plot relative mass distribution, i.e. m_pred/m_true
    Normalization can be applied by setting 'normalize=True'
    """
    print("--Plotting relative mass distribution ")
    i = np.where(Y_val_pred>0)
    rela = Histogram(Y_val_pred[i]/Y_val[i],label=r'$m_{pred}>0$ GeV', colour="red")
    i = np.where((Y_val_pred>100) & (Y_val_pred<150))
    rela2 = Histogram(Y_val_pred[i]/Y_val[i],label=r'$100<m_{pred}<150$ GeV', colour="green")

    #initialise Histogram plot
    plot = HistogramPlot(
        xlabel=r'$m_{pred}/m_{true}$',
        ylabel="Normalised number of jets",
        bins=50,
        bins_range=(0,2),
        norm=normalize,
        #logy=1,
        figsize=(6, 5),
        #n_ratio_panels=1,
        atlas_second_tag="$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, \n$|\eta|$ < 1.97", #Not 100 GeV?
    )
    
    plot.add(rela, reference=False)
    plot.add(rela2)
    plot.draw()
    
    outputname="rela_m_distr.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_error_vs_m(Y_val, Y_val_pred, outputdir='.'):
    """
    Plot the rmse as a function of the true mass.
    """
    print("--Plotting rmse vs m_true ")
    e=[]
    bins = np.arange(50,200,10)+5
    for i in range(15):
        j = np.where((Y_val>50 + i*10)&(Y_val<50+(i+1)*10))
        e.append(mean_squared_error(Y_val[j], Y_val_pred[j])**0.5)
    ee=np.ones(15)*5

    plot = plt.figure(figsize=[6,5])
    plt.errorbar(bins, np.array(e), xerr=ee,fmt='.', label='Transformer')
    #x=[3.,300.]
    x=[50.,200.]
    plt.plot(x, [10, 10], label='Max sensitivity UFO @ $m_H$')
    plt.plot(x, [0.6, 0.6], label='Sensitivity 2012')
    plt.plot(x, [0.17, 0.17], label='Sensitivity 2022')
    plt.xlim(x)
    #plt.yscale('log')
    plt.legend()
    plt.xlabel(r'$m_{true}$ [GeV]')
    plt.ylabel(r'RMSE [GeV]')
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")
    #plot.draw()

    outputname="rmse_vs_m.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_error_vs_pt(Y_val, Y_val_pred, jet_pt, outputdir='.'):
    """
    Plot the rmse as a function of pt.
    """
    print("--Plotting rmse vs pt ")
    e=[]
    bins = np.arange(250,1000,50)+25
    for i in range(15):
        j = np.where((jet_pt>250 + i*50)&(jet_pt<250+(i+1)*50))
        try: 
            e.append(mean_squared_error(Y_val[j], Y_val_pred[j])**0.5)
        except:
            print("eta bins without events")
            e.append(0)
    ee=np.ones(15)*25

    plot = plt.figure(figsize=[6,5])
    plt.errorbar(bins, np.array(e), xerr=ee,fmt='.', label='Transformer')
    #x=[3.,300.]
    x=[250.,1000.]
    plt.plot(x, [10, 10], label='Max sensitivity UFO @ $m_H$')
    plt.plot(x, [0.6, 0.6], label='Sensitivity 2012')
    plt.plot(x, [0.17, 0.17], label='Sensitivity 2022')
    plt.xlim(x)
    #plt.yscale('log')
    plt.legend()
    plt.xlabel(r'$p_T}$ [GeV]')
    plt.ylabel(r'RMSE [GeV]')
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")
    #plot.draw()

    outputname="rmse_vs_pt.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_error_vs_eta(Y_val, Y_val_pred, jet_eta, outputdir='.'):
    """
    Plot the rmse as a function of |eta|.
    """
    print("--Plotting rmse vs |eta| ")
    e=[]
    bins = np.arange(0,2,0.1)+0.05
    for i in range(20):
        j = np.where((jet_eta>i*0.1)&(jet_eta<(i+1)*0.1))
        try:
            e.append(mean_squared_error(Y_val[j], Y_val_pred[j])**0.5)
        except:
            print("eta bins without events")
            e.append(0)
    ee=np.ones(20)*0.05

    plot = plt.figure(figsize=[6,5])
    plt.errorbar(bins, np.array(e), xerr=ee,fmt='.', label='Transformer')
    #x=[3.,300.]
    x=[0.,2.]
    plt.plot(x, [10, 10], label='Max sensitivity UFO @ $m_H$')
    plt.plot(x, [0.6, 0.6], label='Sensitivity 2012')
    plt.plot(x, [0.17, 0.17], label='Sensitivity 2022')
    plt.xlim(x)
    #plt.yscale('log')
    plt.legend()
    plt.xlabel(r'$|\eta|$ [GeV]')
    plt.ylabel(r'RMSE [GeV]')
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")
    #plot.draw()

    outputname="rmse_vs_eta.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_identity_dist(Y_val, Y_val_pred, outputdir='.'):
    """
    Plot the true mass vs the predicted mass
    """
    print("--Plotting identity distribution ")
    l=min([5000,len(Y_val)])
    Y_val = Y_val[:l]
    Y_val_pred = Y_val_pred[:l]

    f = plt.figure(figsize=[6,5])
    plt.scatter(Y_val, Y_val_pred, marker='.', alpha=0.2)
    x=[50.,200.]
    y=[30.,220.]
    plt.plot(x, x, color='black', linewidth=0.1)
    plt.xlim(x)
    plt.ylim(y)
    plt.xlabel(r'$m_{true}$ [GeV]')
    plt.ylabel(r'$m_{pred}$ [GeV]')
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")
    #plot.draw()
    
    outputname="mtrue_vs_mpred.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    f.savefig(strFile)

def plotter(Y_val, Y_val_pred, jet_pt, jet_eta, outputdir='.'):
    """
    Plot the rmse as a function of pt and eta.
    """
    print("--Plotting rmse vs pt and eta")
    e=np.zeros((10,15))
    for i in range(15):
        for k in range(10):
            j = np.where((jet_pt>250 + i*50) & (jet_pt<250+(i+1)*50) & (jet_eta>k*0.2)&(jet_eta<(k+1)*0.2))
            try: 
                e[k,i] = mean_squared_error(Y_val[j], Y_val_pred[j])**0.5
            except:
                print("pt/eta bins without events")
                e[k,i]=0
    print("Average number of jets per bin:", len(Y_val)/10/15)
    bins_pt  = np.arange(250,1000,50)+25
    bins_eta = np.arange(0,2,0.2)+0.1
    bins_eta = [f"{b:.1f}" for b in bins_eta]

    plot = plt.figure(figsize=[8,5])
    s = sb.heatmap(e, annot=True, cmap="RdYlGn_r", vmin=0, xticklabels=bins_pt, yticklabels=bins_eta, square=True )
    s.set_xlabel(r'$p_T$ [GeV]', fontsize=10)
    s.set_ylabel(r'$\eta$', fontsize=10)
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")

    outputname="rmse_vs_pt_eta.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)

def plotter_error_vs_pt_eta(Y_val, Y_val_pred, jet_pt, jet_eta, outputdir='.'):
    """
    Plot the rmse as a function of pt and eta.
    """
    print("--Plotting rmse vs pt and eta")
    e=np.zeros((10,15))
    for i in range(15):
        for k in range(10):
            j = np.where((jet_pt>250 + i*50) & (jet_pt<250+(i+1)*50) & (jet_eta>k*0.2)&(jet_eta<(k+1)*0.2))
            try: 
                e[k,i] = mean_squared_error(Y_val[j], Y_val_pred[j])**0.5
            except:
                print("pt/eta bins without events")
                e[k,i]=0
    print("Average number of jets per bin:", len(Y_val)/10/15)
    bins_pt  = np.arange(250,1050,50)
    bins_eta = np.arange(0,2.2,0.2)
    
    plot = plt.figure(figsize=[8,5])
    p = plt.imshow(e,cmap="RdYlGn_r", vmin=0, extent=[250,1000,0,2], aspect='auto', origin='lower')
    plt.xticks(bins_pt)
    plt.yticks(bins_eta)
    plt.xlabel(r'$p_T$ [GeV]')
    plt.ylabel(r'$\eta$')
    plt.colorbar(p)
    for (k,i),label in np.ndenumerate(e):
        plt.text(bins_pt[i]+25, bins_eta[k]+0.1,round(label),ha='center',va='center')
    plt.title(r"$\sqrt{s}=13$ TeV, Large-R jet 250 < $p_{T}$ < 1000 GeV, $|\eta|$ < 1.97")

    outputname="rmse_vs_pt_eta2.pdf"
    strFile = outputdir+"/"+outputname
    if os.path.isfile(strFile):
        os.remove(strFile)
    plot.savefig(strFile)
#==========================================

def make_plots(out_dir, data):#folder,data
    #logger.info("Calculate discriminants and rejection")
    print("Determine RMSE")
    with h5py.File(data, 'r') as f:
        jet_pt = f['jets']['pt'] / 1000
        jet_mass = f['jets']['mass'] / 1000 #also used as input, this is not UFO jet reco mass
        jet_mass_pred = f['jets']['mH_antikt10_pred'] / 1000 #Self definedc output
        jet_mass_true = f['jets']['R10TruthLabel_R22v1_TruthJetMass'] / 1000 #Possible target value
        jet_eta = np.abs(f['jets']['eta'])
        flav = f['jets']['R10TruthLabel_R22v1']
        #is_QCD = flav == 10
        #is_Hcc = flav == 12
        is_Hbb = flav == 11 #The events are ordered, so using only the first few events does not include Hbb
        #is_Top = flav == 1
        mask = (jet_pt < 1000) & (jet_pt > 250) & (jet_mass_true > 50) & (jet_mass_true < 300) & (is_Hbb) #& (jet_eta < 1.97)
        print("number of Hbb events:", sum(is_Hbb))
        
    #jet_mass_pred=jet_mass########Need to remove this when I can run with the correct data and ADD the isHbb mask

    jet_mass = jet_mass[mask]
    jet_mass_true = jet_mass_true[mask]
    jet_mass_pred = jet_mass_pred[mask]
    jet_pt = jet_pt[mask]
    jet_eta = jet_eta[mask]
    print(jet_mass_true.shape, jet_mass_pred.shape)

    #Entire mass range
    rms = mean_squared_error(jet_mass_true, jet_mass_pred)**0.5
    print("--RMS =",round(rms, 2),"GeV")

    #Higgs range
    i = (jet_mass_pred>100) & (jet_mass_pred<150)
    if sum(i)!=0:
        rms = mean_squared_error(jet_mass_true[i], jet_mass_pred[i])**0.5
        print("--RMS =",round(rms, 2),r'GeV, for $100<m_{pred}<150$ GeV')
    else:
        print("No data in Higgs range")

    plotter_mass_dist(jet_mass_true, jet_mass_pred, jet_mass, normalize=True, outputdir=out_dir)
    plotter_relam_dist(jet_mass_true, jet_mass_pred, normalize=True, outputdir=out_dir)
    plotter_error_vs_m(jet_mass_true, jet_mass_pred, outputdir=out_dir)
    plotter_error_vs_pt(jet_mass_true, jet_mass_pred, jet_pt, outputdir=out_dir)
    plotter_error_vs_eta(jet_mass_true, jet_mass_pred, jet_eta, outputdir=out_dir)
    plotter_identity_dist(jet_mass_true, jet_mass_pred, outputdir=out_dir)
    plotter_error_vs_pt_eta(jet_mass_true, jet_mass_pred, jet_pt, jet_eta, outputdir=out_dir)
    #pt cut 2D
    print("--Done!")

#make_plots("/user/wmorren/project/salt/salt/logs/reg_j3_t100_H2_D32_L1_n1_e2_d1e-1_20230227_153922", "/user/wmorren/project/salt/salt/logs/reg_j3_t100_H2_D32_L1_n1_e2_d1e-1_20230227_153922/ckpts/epoch=000-val_loss=4.32394__test_Mix.h5")
